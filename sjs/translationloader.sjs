class TranslationLoader {

    constructor() {
        this.trans = null;
    }

    loadTranslation() {
        if(this.trans == null) {
            this.trans = de.DE();
        }

        console.log("Created translation object.");
        return this.trans;
    }

}
