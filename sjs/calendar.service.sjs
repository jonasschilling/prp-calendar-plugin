/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class CalendarService {

    constructor() {
        this.token = "Bearer " + window.localStorage.getItem('access_token');
        this.domain = "http://prp.error-cloud.com:8080";
        this.logger = new Logger("CalendarService");
    }

    /******************************
     Calendar
     *****************************/

    async loadAllCalendars() {
        let that = this;
        that.logger.log("Loading all calendars");

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar",
                    method: "GET",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        that.logger.log("Load all calendar request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (error) {
                        that.logger.warn("Getting all calendars request failed: jqXHR: " + JSON.stringify(error));
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async createNewCalendar(calendar) {
        let that = this;

        that.logger.log("Creating new calendar" + JSON.stringify(calendar));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/create",
                    method: "POST",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(calendar),
                    success: function (data) {
                        that.logger.log("Create calendar request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Create calendar request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async loadCalendarById(calendarId) {
        let that = this;

        that.logger.log("Loading calendar: " + JSON.stringify(calendarId));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId,
                    method: "GET",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        that.logger.log("Load calendar by Id request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Load calendar by Id request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async updateCalendarById(calendar) {
        let that = this;

        that.logger.log("Deleting calendar: " + JSON.stringify(calendar));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendar.id,
                    method: "PUT",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(calendar),
                    success: function (data) {
                        that.logger.log("Update calendar request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Update calendar request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async deleteCalendarById(calendarId) {
        let that = this;

        that.logger.log("Deleting calendar: " + calendarId);

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId,
                    method: "DELETE",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        that.logger.log("Delete calendar request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Delete calendar request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    /******************************
     Events
     *****************************/

    async loadAllEvents() {
        let that = this;

        that.logger.log("Loading all events");

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/event/list",
                    method: "GET",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        that.logger.log("Load all Events request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Load all Events request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });


    }

    // TODO: Anpassen dass per calendar geladen wird
    // date formate: dd-MM-yyyy HH:mm
    async loadAllEventsForDate(date) {
        let that = this;

        that.logger.log("Loading all events");

        await $.ajax({
            url: this.domain + "/api/0.1/calendar/event/list",
            method: "GET",
            headers: {
                "Authorization": this.token
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(date),
            success: function (data) {
                that.logger.log("Load all Events for Date request was successful. Data: " + JSON.stringify(data));
                return data;
            },
            error: function (data, textStatus, errorThrown) {
                that.logger.error("Load all Events for Date request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                return [];
            }
        });
    }

    async createEventInCalendar(calendarId, event) {
        let that = this;

        that.logger.log("Creating event for calendar: " + calendarId + " " + JSON.stringify(event));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId + "/event",
                    method: "POST",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(event),
                    success: function (data) {
                        that.logger.log("Create event request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Create event request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async updateExistingEvent(calendarId, event) {
        let that = this;

        that.logger.log("Updating event for calendar: " + calendarId + " " + JSON.stringify(event));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId + "/event/" + event.id,
                    method: "PUT",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(event),
                    success: function (data) {
                        that.logger.log("Update event request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Update event request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });


    }

    async deleteExistingEvent(calendarId, event) {
        let that = this;

        that.logger.log("Deleting event for calendar: " + calendarId + " " + JSON.stringify(event));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId + "/event/" + event.id,
                    method: "DELETE",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        that.logger.log("Delete event request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Delete event request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    /******************************
     Reminder
     ******************************/

    async createReminderForEvent(calendarId, eventId, date) {
        let that = this;

        that.logger.log("Creating reminder for calendar: " + calendarId + " EventID: " + eventId + " Date: " + date);

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId + "/event/" + eventId + "/reminder",
                    method: "POST",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(date),
                    success: function (data) {
                        that.logger.log("Create event reminder request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Create event reminder request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async deleteReminderForEvent(calendarId, eventId, date) {
        let that = this;

        that.logger.log("Deleting reminder for calendar: " + calendarId + " EventID: " + eventId + " Date: " + date);

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId + "/event/" + eventId + "/reminder/" + date,
                    method: "DELETE",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        that.logger.log("Delete event reminder request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        that.logger.error("Delete event reminder request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

}
